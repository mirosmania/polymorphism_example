package ru.javacourse.component;


import ru.javacourse.relation.ManagerFrame;

import javax.swing.JFrame;

public class Starter {
    public static void main(String[] args) {
        OvalComponent oc=new OvalComponent();
        oc.setGap(10);

        ManagerFrame omf =new ComponetFrame(oc);
        omf.setVisible(true);

        JFrame slaveFrame=new JFrame();
        slaveFrame.setBounds(500, 200, 200, 200);
        slaveFrame.setVisible(true);
        slaveFrame.add(oc);

        omf.setSlave(slaveFrame);
    }
}
