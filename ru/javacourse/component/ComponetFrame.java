package ru.javacourse.component;


import ru.javacourse.relation.ManagerFrame;

import javax.swing.JComponent;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ComponetFrame extends ManagerFrame{

    //Почему если вместо OvalComponent везде кроме(*) поставить тип JComponent то-oc.setGap(15) не работает-метод не доступен.
    private OvalComponent component;


    public ComponetFrame(OvalComponent component) {
        super(component);
        setComponent(component);
    }

    private void setComponent(OvalComponent component){
        this.component=component;
    }

    public OvalComponent getComponent() {
        return component;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();

        OvalComponent oc=getComponent();//(*)
        if (LEFT.equals(command))
            oc.setGap(oc.getGap() + STEP);
        else if (RIGHT.equals(command)){
            oc.setGap(oc.getGap() - STEP);
            oc.setOvalW(oc.getOvalW()+(2*STEP));
            oc.setOvalH(oc.getOvalH() + (2*STEP));
        }

        oc.setCommand(command);
        oc.paintComponent(oc.getGraphics());
        getSlave().getComponent(0).repaint();

    }
}
