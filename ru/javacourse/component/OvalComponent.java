package ru.javacourse.component;

import javax.swing.JComponent;
import java.awt.Graphics;


public class OvalComponent extends JComponent{
    private int gap=10;


    private  int ovalW;


    private  int ovalH;


    private String command;

    @Override
    protected void paintComponent(Graphics g) {
        if (ComponetFrame.RIGHT.equals(getCommand()))
            g.drawOval(getGap(),getGap(),getOvalW(),getOvalH());
        else{
            setOvalW(getWidth() - (2*getGap()));
            setOvalH(getHeight() - (2*getGap()));
            g.drawOval(getGap(),getGap(),getOvalW(),getOvalH());
        }

    }

    public void setOvalW(int ovalW){
        this.ovalW=ovalW;
    }

    public int getOvalW() {
        return ovalW;
    }

    public int getOvalH() {
        return ovalH;
    }

    public void setOvalH(int ovalH) {
        this.ovalH = ovalH;
    }


    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }


    protected int getGap() {
        return gap;
    }

    public void setGap(int gap) {
        this.gap = gap;
    }

}
