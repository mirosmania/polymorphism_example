package ru.javacourse.relation;


import javax.swing.*;
import java.awt.Rectangle;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ManagerFrame extends JFrame implements ActionListener {
    public JFrame slave;
    private static final String UP = "UP";
    private static final String DOWN = "DOWN";
    public static final String LEFT = "LEFT";
    public static final String RIGHT = "RIGHT";
    public static final int STEP = 10;

    public void setSlave(JFrame slave) {
        this.slave = slave;
    }

    protected JFrame getSlave(){
        return  slave;
    }


    public ManagerFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(200, 200, 200, 200);
        DrawLeftRightBtns();
        DrawUpDownBtns();
    }

    public ManagerFrame(JComponent component){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(200, 200, 200, 200);
        DrawLeftRightBtns();
    }



    protected void DrawUpDownBtns() {
        JButton up = new JButton("UP");//надпись на кнопке
        up.addActionListener(this);//тот кто слушает событие по этой кнопке
        up.setActionCommand(UP);//команда-флаг
        add(up, BorderLayout.NORTH);//положи кнопку на форму

        JButton down = new JButton("DOWN");
        down.addActionListener(this);
        down.setActionCommand(DOWN);
        add(down, BorderLayout.SOUTH);
    }

    protected void DrawLeftRightBtns() {
        JButton left = new JButton("LEFT");
        left.addActionListener(this);
        left.setActionCommand(LEFT);
        add(left, BorderLayout.WEST);

        JButton right = new JButton("RIGHT");
        right.addActionListener(this);
        right.setActionCommand(RIGHT);
        add(right, BorderLayout.EAST);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Rectangle rect = slave.getBounds();
        String command = e.getActionCommand();
        if (UP.equals(command))
            rect.y -= STEP;
        else if (DOWN.equals(command))
            rect.y += STEP;
        else if (LEFT.equals(command))
            rect.x -= STEP;
        else if (RIGHT.equals(command))
            rect.x += STEP;

        slave.setBounds(rect);
    }
}

