package ru.javacourse.relation;


import javax.swing.JFrame;

public class Starter {
    public static void main(String[] args) {
        ManagerFrame mf=new ManagerFrame();
        mf.setVisible(true);

        JFrame slaveFrame=new JFrame();
        slaveFrame.setBounds(500, 200, 200, 200);
        slaveFrame.setVisible(true);

        mf.setSlave(slaveFrame);
    }
}
